#!/usr/bin/env python
# coding: utf-8

# # Kapitola 3 - Množiny

# ## Množina, prvek množiny

# ```{admonition} Definice 3.1
# *Množina* je takový soubor objektů, kdy o každém objektu můžeme rozhodnout, zda patří nebo nepatří do uvažovaného souboru objektů.
# ```
# 

# ```{admonition} Poznámka 3.1
# :class: hint
# Množiny budeme značit zpravidla písmeny velké abecedy $A, B, C, D ...$; a prvky (objekty) patřící do množiny písmeny malé abecedy $a, b, c, d, ...$. 
# 
# Skutečnost, že $a$ je prvkem množiny $M$ zapíšeme pomocí $a\in M$ a budeme číst „*prvek $a$ patří do množiny $M$*“. 
# 
# Pro každou množinu $M$ nastane právě jedna ze dvou možností: $a\in M, a\notin M$.
# ```       

# ```{admonition} Poznámka 3.2
# :class: hint
# **Množiny určujeme dvojím způsobem**
# 
# a) *Výčtem prvků*: do složených závorek vypíšeme všechny prvky, ze kterých se množina skládá, například:
# 
# $M = \{$ chléb, mléko, čokoláda $\}$
# 
# $L = \{5, 6, 7, 8, 9, 10\} $
# 
# b) *Charakteristickou vlastností*: stanovíme kritérium příslušnosti pro všechny prvky množiny: 
# 
# $M = \{$ potraviny, které koupil Jaroslav$\}$
# 
# $L = x\in \mathbb{N}; 5 \leq x \leq 10$
# ```       

# ```{admonition} Poznámka 3.3
# :class: hint
# Při sestavování množin vycházíme zpravidla z určité předem stanovené množiny (například $\mathbb{N}_0$ ), které říkáme *základní množina*.
# 
# **Úmluva** 
# $\mathbb{N}_0$ ... množina všech přirozených čísel $\{0,1,2,3,\ldots\}$ 
# $\mathbb{N}$ ... množina všech přirozených čísel bez nuly $\{1,2,3, \ldots\}$ 
# $\mathbb{Z}$ ... množina všech celých čísel
# $\mathbb{Q}$ ... množina všech racionálních čísel 
# $\mathbb{R}$ ... množina všech reálných čísel
# 
# Důležitou množinou je *množina prázdná*, která neobsahuje žádný prvek. Značí se $\emptyset$.
# ```       
# 

# ### Příklad 3.1
# 
# Přečtěte symbolické zápisy a určete množiny výčtem prvků: 
# 
# a) $A=\{ x\in \mathbb{N}_0, x^2=2\}. 
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# $A=\{ x\in \mathbb{N}_0, x^2=2\}=\{ 0, 1\}. 
# 
# ```   
# 
# b) $B= \{x\in \mathbb{N}, 2 x+3 < 9\}$
# 
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# $B= \{x\in \mathbb{N}, 2 x+3 < 9\}=\{1, 2\}$
# ```   
# 
# c) Zapište množinu všech samohlásek ve slově pravítko.
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# $P=\{$a,í, o $\}$
# ```   

# ```{admonition} Poznámka 3.4
# :class: hint
# Zabýváme se tzv. *intuitivní teorií množin*, která je pro drtivou většinu našich úvah dostatečná. 
# 
# Nicméně při některých úvahách s nekonečnými množinami můžeme narazit na paradoxy (například tzv. *Russelův paradox* - uvažuje se množina $A$ všech množin, které nejsou obsaženy samy v sobě jako prvek. Ke sporu vede jak úvaha, že $A$ je sama svým prvkem, tak i úvaha, že $A$ není sama svým prvkem). 
# 
# Proto se přistoupilo začátkem 20. století k tzv. *axiomatické teorii množin*.
# ```       

# ## Inkluze množin, rovnost množin

# ```{admonition} Definice 3.2
# Jestliže každý prvek, který je prvkem množiny $A$, je prvkem množiny $B$, říkáme, že množina $A$ je *podmnožinou* množiny $B$, nebo že množina $B$ je *nadmnožinou* množiny $A$ a zapisujeme $A \subset B$ nebo $B \supset A$. 
# 
# Tento vztah se nazývá *inkluze množin*.
# ```

# ```{admonition} Poznámka 3.4
# :class: hint
# Množiny znázorňujeme Vennovým diagramem. *Vennův diagram* je tvořený uzavřenými křivkami, přičemž body uvnitř křivky představují prvky dané množiny a body venku prvky, které do množiny nepatří.
# 
# Nicméně při některých úvahách s nekonečnými množinami můžeme narazit na paradoxy (například tzv. *Russelův paradox* - uvažuje se množina $A$ všech množin, které nejsou obsaženy samy v sobě jako prvek. Ke sporu vede jak úvaha, že $A$ je sama svým prvkem, tak i úvaha, že $A$ není sama svým prvkem). 
# 
# Proto se přistoupilo začátkem 20. století k tzv. *axiomatické teorii množin*.
# ```       
# 
# 

# ### Příklad 3.2
# 
# Uvažujme základní množinu $Z = \{ 1, 2 ,3, 4, 5, 6, 7, 8, 9, 10\}$
# 
# Pomocí Vennova diagramu zakreslete množiny 
# 
# $A=\{x\in \mathbb{Z}, x | 3 \}$,
# 
# $B=\{x\in \mathbb{Z}, x | 6 \}$.
# 
# Vyznačte příslušné prvky množin.
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# $A=\{ 1, 3\}, B=\{1, 2, 3, 6\}$
# 
# ![fishy](images/img03-01.png)
# 
# Platí $A\subset B$.
# ```   
# 

# ```{admonition} Poznámka 3.5
# :class: hint
# Pro libovolnou množinu $M$ platí inkluze $\emptyset \subset M, M \subset M$. Rovněž platí $\emptyset \subset \emptyset$.
# ```       
# 

# ```{admonition} Poznámka 3.6
# :class: hint
# Jestliže každý prvek množiny $A$ je prvkem množiny $B$ a jestliže současně také každý prvek množiny $B$ je prvkem množiny $A$, pak jsou si množiny $A, B$ *rovny*, píšeme $A = B$. Jestliže si množiny $A, B$ nejsou rovny, píšeme $A \neq B$.
# 
# Znázornění Vennovým diagramem $A = B$
# 
# ![fishy](images/img03-02.png)
# 
# Znázornění Vennovým diagramem $A \subset B$
# 
# ![fishy](images/img03-03.png)
# 
# ```       
# 

# ```{admonition} Poznámka 3.7
# :class: hint
# Splňují-li množiny $A, B$ tyto podmínky: $A\subset B \land A \neq B$, říkáme, že množina $A$ je *vlastní podmnožinou* množiny $B$. 
# 
# Pokud množina $A$ není podmnožinou množiny $B$, píšeme $A\not\subset B$.
# ```    

# ```{admonition} Definice 3.3
# *Potenční systém množiny* $M$ (značíme $\mathcal{P}(M)$) je množina všech podmnožin množiny $M$. Počet prvků potenční množiny  $\mathcal{P}(M)$ je $2^n$, kde $n$ je počet prvků množiny $M$.
# ```
# 
# 

# ### Příklad 3.3
# 
# Sestrojte potenční množinu množiny $\{1, a, b\}$.
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# $\mathcal{P}(M)=\{\emptyset, \{1\}, \{a\},  \{b\}, \{1, a\},  \{1, b \},  \{a, b \}, M\}$
# ```   
# 
