#!/usr/bin/env python
# coding: utf-8

# # Kapitola 4 - Operace s množinami

# ## Doplněk množiny

# ```{admonition} Definice 4.1
# *Doplněk množiny* $A$ je taková množina, která obsahuje právě ty prvky ze základní množiny $Z$, jež nepatří do množiny $A$. Značíme $A'$.
# 
# $A'=\{ x\in Z, x\notin A\}$
# 
# 
# ![fishy](images/img04-01.png)
# ```
# 

# ```{admonition} Příklad 4.1
# :class: hint
# Nechť $Z$ je množina $\mathbb{N}$ a $A$ je množina všech sudých přirozených čísel. Poté $A'$ je množina všech lichých přirozených čísel.
# ```       
# 
# 

# ## Sjednocení dvou množin

# ```{admonition} Definice 4.2
# Sjednocení dvou množin $A, B (A \subset Z, B\subset Z)$ je množina $S$ obsahující ty prvky ze základní množiny $Z$, které patří do alespoň jedné z množin $A, B$. Píšeme $S = A \cup B$.
# 
# 
# ![fishy](images/img04-02.png)
# ```
# 

# ```{admonition} Příklad 4.2
# :class: hint
# Je dána základní množina $Z = \{1,2,3,4,5,6,7,8,9,10,11,12\}$ a množiny $A = \{2,4,6\}$ , $B =\{ 7,9,10\}$. Určete sjednocení množin $A, B$.
# 
# Řešení: $A\cup B = \{2,4,6,7,9,10\}$.
# ```       
# 
# 

# ## Průnik dvou množin

# ```{admonition} Definice 4.3
# *Průnik dvou množin* $A, B (A \subset Z, B \subset Z)$ je množina $P$, která obsahuje ty prvky ze základní množiny $Z$, které patří do množiny $A$ a současně do množiny $B$. Píšeme $P= A \cap B$.
# 
# 
# ![fishy](images/img04-03.png)
# ```
# 

# ```{admonition} Příklad 4.3
# :class: hint
# Je dána základní množina $Z = \{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12\}$ množiny $A = \{1, 3, 4, 7, 10\} , B = \{2, 3, 5, 8, 12\}$. Určete jejich průnik. 
# 
# Řešení: $A \cap B= \{3\}$. 
# ```       
# 
# 

# ```{admonition} Definice 4.4
# Množiny $A, B$, jejichž průnik je množina prázdná, se nazývá *disjunktní*.
# ```
# 

# ## Rozdíl dvou množin

# ```{admonition} Definice 4.5
# Rozdíl dvou množin $A, B$ je množina, která obsahuje všechny prvky množiny $A$, které nepatří do množiny $B$. Značíme $A \setminus B$.
# 
# 
# ![fishy](images/img04-04.png)
# ```
# 
# 

# ```{admonition} Příklad 4.4
# :class: hint
# Je dána základní množina $Z = \{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12\}$ a množiny $A = \{2, 4, 6, 8, 10,12\}, B = \{4, 5, 6, 7, 8\}$. Určete rozdíl množin $A, B$.
# 
# Řešení: $A \setminus B = \{2, 10, 12\}$
# ```       
# 
# 

# ## Symetrický rozdíl dvou množin

# ```{admonition} Definice 4.6
# Symetrický rozdíl dvou množin $A, B$ je množina, která obsahuje právě ty prvky ze základní množiny $Z$, jež patří právě do jedné z množin $A, B$. Značíme $A\triangle B$.
# 
# ![fishy](images/img04-05.png)
# ```
# 
# 

# ```{admonition} Příklad 4.5
# :class: hint
# Je dána základní množina $Z = \{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12\}$ a množiny $A =\{ 2, 4, 6, 8, 10, 12\}, B = \{3, 6, 9, 12\}$. Určete symetrický rozdíl množin $A, B$.
# 
# Řešení: $A\triangle B = \{2, 3, 4, 8, 9, 10, 12\}$.
# ```       
# 
# 

# ## Vlastnosti operací s množinami

# ```{admonition} Poznámka 3.6
# :class: hint
# Nechť je dána základní množina $Z$ a její libovolné podmnožiny $A, B, C$. Pak platí
# 
# 1. $A \subset (A \cup B), B \subset (A \cup B)$,
# 
# 2. Jestliže platí $A\subset B$, pak platí $A \cup B = B$,
# 
# 3. $A\cup Z = Z$, $A \cup A = A$, $\emptyset\cup A=A$,
# 
# 4. Komutativnost
# 
#     a. sjednocení: $A\cup B = B \cup A$
#     
#     b. průniku: $A\cap B = B \cap A$
#     
# 5. Asociativnost
#     
#     a. sjednocení: $(A\cup B)\cup C = A \cup (B \cup C)$
#     
#     b. průniku: $(A\cap B)\cap C = A \cap (B \cap C)$
#     
# 6. Distributivnost
#     
#     a. sjednocení vzhledem k průniku $(A \cap B) \cup C = (A \cup C) \cap (B \cup C)$
#     
#     b. průnik vzhledem ke sjednocení $(A \cup B) \cap C = (A \cap C) \cup (B \cap C)$
#     
# 7. $A\cup A' = Z, A \cap A' = \emptyset,$
#    
#    $\emptyset' = Z, Z' = \emptyset,$
#    
#    $\emptyset \cup \emptyset = \emptyset, Z \cap Z = Z,$
#    
#    $\emptyset \cup Z = Z \cup Z = Z, Z \cap \emptyset = \emptyset \cap \emptyset '\emptyset$ 
#    
# 8. De Morganovy zákony
#    $(A \cup B)' = A' \cap B'$
#    
#    $(A \cap B)' = A' \cup B'$
#    
# 9. Jestliže $A \neq  B$, potom  $A\setminus B \neq B\setminus A$
# 
# 10. Jestliže $A \subset B$, potom $A \setminus B = \emptyset$
#   
# ```       
# 

# ### Příklad 4.6
# 
# Na množinách $Z =\{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20\}$,  $A=\{x\in \mathbb{Z}, x| 36\}$, $B = \{x\in\mathbb{Z};  5 \leq x \leq 12\}$ ilustrujte platnost de Morganových zákonů.
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# 
# Ověření zákona $\boldsymbol{(A \cup B)' = A' \cap B'}$:
# 
# $A = \{1, 2, 3, 6, 9, 12, 18\}$,
# 
# $B = \{5, 6, 7, 8, 9, 10, 11, 12\}$,
# 
# $(A \cup B) =\{  1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 18\}$,
# 
# $\boldsymbol{(A \cup B)' =\{ 4, 13, 14, 15, 16, 17, 19, 20\}}$,
# 
# $A' = \{4, 5, 7, 8, 10, 11, 13, 14, 15, 16, 17, 19, 20\} $,
# 
# $B' = \{1, 2, 3, 4, 13, 14, 15, 16, 17, 18, 19, 20\} $,
# 
# $\boldsymbol{A' \cap B' = \{4, 13, 14, 15, 16, 17, 19, 20\}}$.
# 
# 
# Ověření zákona $\boldsymbol{(A \cap B)' = A' \cup B'}$:
# 
# $A = \{1, 2, 3, 6, 9, 12, 18\}$,
# 
# $B = \{5, 6, 7, 8, 9, 10, 11, 12\}$,
# 
# $(A \cap B) =\{  6, 9, 12 \}$,
# 
# $\boldsymbol{(A \cap B)' =\{1, 2, 3, 4, 5, 7, 8, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20\}}$,
# 
# $A' = \{4, 5, 7, 8, 10, 11, 13, 14, 15, 16, 17, 19, 20\} $
# 
# $B' = \{1, 2, 3, 4, 13, 14, 15, 16, 17, 18, 19, 20\} $
# 
# $\boldsymbol{A' \cup B' =\{1, 2, 3, 4, 5, 7, 8, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20\}}$.
#  
# ```   
# 
