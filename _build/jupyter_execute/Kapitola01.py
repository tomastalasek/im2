#!/usr/bin/env python
# coding: utf-8

# # Kapitola 1 - Základní pojmy výrokové logiky

# ## Výrok

# ```{admonition} Definice 1.1
# *Výrokem* rozumíme každé sdělení, o kterém můžeme rozhodnout, zda je pravdivé či nepravdivé.
# ```

# ```{admonition} Poznámka 1.1
# :class: hint
# Každý výrok má právě jednu *pravdivostní hodnotu*: buď je pravdivý (pravdivostní hodnota $1$) nebo je nepravdivý (pravdivostní hodnota $0$).
# ```       

# ```{admonition} Příklad 1.1
# :class: hint
# Rozhodněte, která z následujících sdělení jsou výrokem . Pokud se jedná o výrok, určete jeho pravdivostní hodnotu.
# 
# 1. Paříž je hlavní město Slovenska. 
# 
# 2. Doneste mi prosím noviny. 
# 
# 3. Číslo 125 je dělitelné 25. 
# 
# 4. Číslo 171 je prvočíslo
# 
# 5. Zákaz vstupu se zmrzlinou. 
# 
# 6. Je dneska pátek? 
# 
# 7. $5^2 = 3^2 + 4^2$ 
# 
# 8. $a^2 + b^2 = c^2$
# ```       

# ```{admonition} Poznámka 1.2
# :class: hint
# Výroky nejsou například rozkazy, příkazy, otázky. Výroky často označujeme písmeny, například: 
# 
# *výrok A*: Bitva u Lipan proběhla v roce 1434.
# ```       

# ## Negace výroku

# ```{admonition} Definice 1.2
# Připojíme-li před určitý výrok A slova „není pravda, že“, popř. provedeme-li stylistické úpravy výroku A mající týž význam jako uvedená slova, dostáváme negaci výroku, kterou označíme $A'$.
# ```

# ```{admonition} Příklad 1.2
# :class: hint
# Uvažujme výrok A: Listopad má 30 dní. 
# 
# Pravdivostní hodnota výroku A je 1. 
# 
# Dva příklady negace výroku $A$:
# - $A'$: Není pravda, že listopad má $30$ dní. 
# - $A'$: Listopad nemá $30$ dní. 
# 
# Pravdivostní hodnota výroku $A'$ je $0$.
# ```       

# ```{admonition} Příklad 1.3
# :class: hint
# Příklad Uvažujme výrok $B$: Čtverec má $5$ vrcholů. 
# 
# Pravdivostní hodnota výroku $B$ je $0$.
# 
# Dva příklady negace výroku $B$:
# 
# - $B'$: Není pravda, že čtverec má $5$ vrcholů. 
# - $B'$: Čtverec nemá $5$ vrcholů. 
# 
# Pravdivostní hodnota výroku $B'$ je $1$.
# ```       

# ```{admonition} Poznámka 1.3
# :class: hint
# **Pozor:** Výrok „Čtverec má $4$ vrcholy.“ není negací výroku $B$.
# ```       

# ```{admonition} Poznámka 1.4
# :class: hint
# Je-li výrok $A$ pravdivý, je jeho negace $A'$ nepravdivá. Je-li výrok $A$ nepravdivý, je jeho negace $A'$ pravdivá.
# 
# 
# **Tabulka pravdivostních hodnot negace výroku:**
# 
# | $A$ | $A'$ |
# |-----|------|
# | 0   | 1    |
# | 1   | 0    |
# ```       

# ## Složené výroky
# 
# Složené výroky vznikají ze dvou nebo více výroků. Tvoříme je pomocí tzv. *výrokotvorných spojek*, které zapisujeme zvláštními symboly zvanými *funktory*.

# ### Konjunkce výroků

# ```{admonition} Definice 1.3
# Spojíme-li dva výroky spojkou „a“ („a současně“, „a zároveň“), dostaneme složený výrok, kterému budeme říkat *konjunkce*.
# ```

# ```{admonition} Příklad 1.4
# :class: hint
# *Výrok $A$:* Číslo $18$ je dělitelné $3$.
# *Výrok $B$:* Číslo $18$ je dělitelné $2$.
# 
# Konjunkce výroků $A$ a $B$: Číslo $18$ je dělitelné $3$ a číslo $18$ je dělitelné $2$. 
# 
# Stručnější vyjádření: Číslo $18$ je dělitelné $3$ a $2$.
# ```       

# ```{admonition} Poznámka 1.5
# :class: hint
# Značení konjunkce $A,B$: $A \land B$ (funktorem konjunkce je $\land$).
# 
# 
# **Tabulka pravdivostních hodnot konjunkce:**
# 
# | $A$ | $B$ | $A\land B$ |
# |-----|-----|-----------|
# | 0   | 0   | 0         |
# | 0   | 1   | 0         |
# | 1   | 0   | 0         |
# | 1   | 1   | 1         |
# 
# Konjunkce je pravdivá, jsou-li pravdivé oba výroky $A, B$.
# ```       
# 
# 
# 

# ```{admonition} Příklad 1.5
# :class: hint
# Určete pravdivostní hodnotu následujících konjunkcí:
# 
# 1. Součet vnitřních úhlů v trojúhelníku je $180^\circ$ a Brno je hlavní město ČR. 
# 
# 2. Absolutní hodnota čísla $-3$ je $3$ a absolutní hodnota čísla $4$ je $-4$.
# ```       

# ### Disjunkce výroků

# ```{admonition} Definice 1.4
# Spojíme-li dva výroky spojkou „nebo“ dostaneme výrok, kterému budeme říkat *disjunkce*.
# ```

# ```{admonition} Příklad 1.6
# :class: hint
# *Výrok $A$:* Číslo $7$ je liché číslo.
# *Výrok $B$:* Číslo $7$ je sudé číslo.
# 
# Disjunkce výroků $A$ a $B$: Číslo $7$ 7 je liché nebo sudé číslo. 
# 
# Pravdivostní hodnota výroku $A$ je $1$, pravdivostní hodnota výroku $B$ je $0$. Pravdivostní hodnota této disjunkce je $1$.    
#     
# ```       

# ```{admonition} Poznámka 1.6
# :class: hint
# Značení disjunkce $A,B$: $A \lor B$ (funktorem konjunkce je $\lor$).
# 
# 
# **Tabulka pravdivostních hodnot disjunkce:**
# 
# | $A$ | $B$ | $A\lor B$ |
# |-----|-----|-----------|
# | 0   | 0   | 0         |
# | 0   | 1   | 1         |
# | 1   | 0   | 1         |
# | 1   | 1   | 1         |
# 
# 
# Disjunkce je pravdivá, je-li pravdivý aspoň jeden z výroků $A, B$.
# 
# ```       
# 
# 
# 

# ```{admonition} Příklad 1.7
# :class: hint
# Určete pravdivostní hodnotu následujících disjunkcí:
# 
# 1. Výrok A: $5 > 5$. 
#    
#    Výrok B: $5 = 5$. 
#    
#    Disjunkce $A,B: 5 \geq 5$. 
#    
# 2. Číslo $21$ je dělitelné $7$ nebo $8$. 
# 
# 3. Číslo $500$ je dvouciferné číslo nebo prvočíslo.
# ```       

# ```{admonition} Poznámka 1.7
# :class: hint
# Spojka „nebo“ se v běžné řeči používá převážně ve vylučovacím smyslu („Odpoledne půjdu do kina nebo běhat.“). 
# 
# Pokud chceme použít spojku „nebo“ ve vylučovacím smyslu v matematice, docílíme toho spojením „buď – nebo“.
# ```       
# 
# 
# 

# ```{admonition} Definice 1.5
# Ostrou disjunkcí dvou výroků $A, B$ rozumíme takový výrok, který je pravdivý právě tehdy, když je právě jeden z výroků $A,B$ pravdivý. Ostrou disjunkci výroků $A, B$ budeme značit $A \veebar B$, kde funktor $\veebar$ odpovídá slovnímu spojení „buď - nebo“.
# ```
# 
# 

# ```{admonition} Poznámka 1.8
# :class: hint
# **Tabulka pravdivostních hodnot ostré disjunkce:**
# 
# | $A$ | $B$ | $A\veebar B$ |
# |-----|-----|-----------|
# | 0   | 0   | 0         |
# | 0   | 1   | 1         |
# | 1   | 0   | 1         |
# | 1   | 1   | 0         |
# 
# 
# Disjunkce je pravdivá, je-li pravdivý aspoň jeden z výroků $A, B$.
# 
# ```    

# ```{admonition} Příklad 1.8
# :class: hint
# Určete pravdivostní hodnotu následujícího výroku:
# 
# Buď je číslo $8$ složené nebo je $13$ prvočíslo.
# ```     

# ### Příklad 1.9
# 
# Zapište pomocí složených výroků:
# 
# 1. Přijde aspoň jeden z mých rodičů. 
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# Přijde moje matka nebo můj otec.
# ```
# 
# 2. Přijde právě jeden z mých rodičů.
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# Buď přijde moje matka nebo můj otec.
# ```

# ### Implikace výroků

# ```{admonition} Definice 1.5
# Spojením dvou výroků pomocí „jestliže - pak“ obdržíme výrok, kterému říkáme *implikace*.
# ```

# ```{admonition} Příklad 1.10
# :class: hint
# *Výrok $A$*: Dnes si půjdu koupit knížku.
# *Výrok $B$*: Zítra si budu číst.
# 
# Implikace výroků $A, B$: Jestliže si dnes půjdu koupit knížku, pak si zítra budu číst.
#     
# ```       

# ```{admonition} Poznámka 1.9
# :class: hint
# Značení implikace $A,B$: $A \Rightarrow B$ (funktorem implikace je $Rightarrow$).
# 
# 
# **Tabulka pravdivostních hodnot disjunkce:**
# 
# | $A$ | $B$ | $A\Rightarrow B$ |
# |-----|-----|-----------|
# | 0   | 0   | 1         |
# | 0   | 1   | 1         |
# | 1   | 0   | 0         |
# | 1   | 1   | 1         |
# 
# ```  
# 

# ```{admonition} Poznámka 1.10
# :class: hint
# V případě implikace se objevují značné rozdíly mezi logikou a běžným hovorovým jazykem.
# 
# V hovorové řeči používáme spojení „jestliže - pak“ pro vyjádření souvislosti jako vztahu mezi příčinou a následkem, např.: *Jestliže sněží, pak se dá sáňkovat*. 
# 
# Ve výrokové logice na souvislost obsahů jednotlivých výroků nehledíme a řídíme se tabulkou pravdivostních hodnot.
# ```       
# 

# ### Příklad 1.11
# 
# Zapište pomocí složených výroků:
# Určete pravdivostní hodnotu následujícího výroku:
# 
# *Jestliže $8$ je prvočíslo, pak dneska sněží.*
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# Výrok „*$8$ je prvočíslo.*“ je nepravdivý. Podle tabulky pravdivostních hodnot je poté implikace „*Jestliže $8$ je prvočíslo, pak dneska sněží.*“ pravdivá bez ohledu na pravdivostní hodnotu výroku „Dneska sněží.“
# ```   

# ```{admonition} Příklad 1.12
# :class: hint
# Určete pravdivostní hodnotu následujících implikací:
# 
# 1. Jestliže je $9$ liché číslo, pak je číslo $125$ dělitelné $5$.
#    
# 2. Jestliže je $\pi$ racionální číslo, pak je $-3$ záporné číslo.
# 
# 3. Jestliže je $-3$ záporné číslo, pak je $\pi$ racionální číslo.
# ```       

# ```{admonition} Poznámka 1.11
# :class: hint
# Implikace $B \Rightarrow A$ se nazývá obrácená implikace vzhledem k implikaci $A \Rightarrow B$.
# 
# 
# **Tabulka pravdivostních hodnot $A \Rightarrow B$, $B \Rightarrow A$**
# 
# | $A$ | $B$ | $A\Rightarrow B$ | $B\Rightarrow A$ |
# |-----|-----|-----------|-----------|
# | 0   | 0   | 1         | 1         |
# | 0   | 1   | 1         | 0         |
# | 1   | 0   | 0         | 1         |
# | 1   | 1   | 1         | 1         |
# 
# ```  
# 

# ### Ekvivalence výroků

# ```{admonition} Definice 1.6
# Spojíme-li dva výroky pomocí „právě tehdy, když“ nebo jiným spojením téhož významu (například „tehdy a jen tehdy, když“), dostaneme složený výrok, kterému říkáme *ekvivalence*.
# ```

# ```{admonition} Příklad 1.13
# :class: hint
# *Výrok $A$*:  *V březnu pojedu do Tater*.
# *Výrok $B$*: *V dubnu pojedu na Šumavu*.
# 
# Ekvivalence výroků $A, B$: *V březnu pojedu do Tater právě tehdy, když v dubnu pojedu na Šumavu*.
# ```       

# ```{admonition} Poznámka 1.12
# :class: hint
# Značení ekvivalence $A,B$: $A \Leftrightarrow B$ (funktorem ekvivalence je $Leftrightarrow$).
# 
# 
# **Tabulka pravdivostních hodnot disjunkce:**
# 
# | $A$ | $B$ | $A\Leftrightarrow B$ |
# |-----|-----|-----------|
# | 0   | 0   | 1         |
# | 0   | 1   | 0         |
# | 1   | 0   | 0         |
# | 1   | 1   | 1         |
# 
# ```  
# 
# 

# ### Příklad 1.14
# 
# Zapište pomocí složených výroků:
# Určete pravdivostní hodnotu následujícího výroku:
# 
# *Číslo $50$ je dělitelné $12$ právě tehdy, když číslo $100$ je větší než číslo $111$.*
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# Výrok „*Číslo $50$ je dělitelné $12$.*“ je nepravdivý. Rovněž výrok „*Číslo $100$ je větší než číslo $111$.*“ je nepravdivý. Podle tabulky pravdivostních hodnot je potom výrok „*Číslo $50$ je dělitelné $12$ právě tehdy, když číslo $100$ je větší než číslo $111$*.“ pravdivý.
# ```   

# ```{admonition} Příklad 1.15
# :class: hint
# Určete pravdivostní hodnotu následujících výroků:
# 
# 1. Číslo $\frac{2}{3}$ je racionální číslo právě tehdy, když $-3$ je kladné číslo..
#    
# 2. Ostrava je hlavní město Číny právě tehdy, když Bratislava je hlavní město Slovenské republiky.
# 
# 3. Odmocnina z *256* je *16* právě tehdy, když odmocnina z *169* je *13*.
# 
# ```
