#!/usr/bin/env python
# coding: utf-8

# # Kapitola 2 - Složené výroky a jejich užití

# ## Abeceda výrokové logiky

# ```{admonition} Definice 2.1
# *Abecedu výrokové logiky* tvoří
# 
# 1. Znaky pro výrokové proměnné $A, B$ a znaky pro konstanty $1, 0$, kde $1$ značí pravdivý výrok a $0$ nepravdivý výrok.
# 
# 2. Znaky pro funktory ${}', \land, \lor, \veebar, \Rightarrow, \Leftrightarrow, \sim$.
# 
# 3. Závorky $( ), [ ], \{ \}$.
# ```
# 

# ```{admonition} Definice 2.2
# Pomocí znaků abecedy výrokové logiky můžeme vytvářet *složené výroky*, kterým říkáme také *výrokové formule*. Například $(A \Rightarrow B) \land C'$.
# ```

# ```{admonition} Poznámka 2.1
# :class: hint
# Složené výroky vyhodnocujeme pomocí tabulky pravdivostních hodnot.
# 
# Pokud složený výrok obsahuje $2$ výroky (výrokové proměnné), pak jeho tabulka pravdivostních hodnot má $2^2 = 4$ řádky. 
# 
# Pokud složený výrok obsahuje $3$ výroky (výrokové proměnné), pak jeho tabulka pravdivostních hodnot má $2^3 = 8$ řádků. 
# 
# Obecně, pokud složený výrok obsahuje $n$ výroků (výrokových proměnných), pak jeho tabulka pravdivostních hodnot má $2^n$ řádků.
# ```       

# ```{admonition} Příklad 2.1
# :class: hint
# *Tabulka pravdivostních hodnot výrokové formule $(A \Rightarrow B) \land C'$:*
# 
# | $A$ | $B$ | $C$ | $A\Rightarrow B$ | $C'$ | $(A \Rightarrow B) \land C'$ |
# |-----|-----|-----|------------------|-----|----|
# | 0   | 0   | 0   | 1                | 1    | 1                            |
# | 0   | 0   | 1   | 1 | 0 | 0 |
# | 0   | 1   | 0   | 1 | 1 | 1 |
# | 0   | 1   | 1   | 1 | 0 | 0 |
# | 1   | 0   | 0   | 0 | 1 | 0 |
# | 1   | 0   | 1   | 0 | 0 | 0 |
# | 1   | 1   | 0   | 1 | 1 | 1 |
# | 1   | 1   | 1   | 1 | 0 | 0 |
# 
# 
# Disjunkce je pravdivá, je-li pravdivý aspoň jeden z výroků $A, B$.
# 
# ```    

# ```{admonition} Poznámka 2.2
# :class: hint
# Pro každou výrokovou formuli nastane právě jedna z těchto možností:
# 
# 1. Výroková formule, která je vždy pravdivá pro jakékoliv pravdivostní hodnoty výrokových proměnných, se nazývá *tautologie*. 
# 
# 2. Výroková formule, která je vždy nepravdivá pro jakékoliv pravdivostní hodnoty výrokových proměnných, se nazývá *kontradikce*. 
# 
# 3. Výroková formule, která je pro některé pravdivostní hodnoty výrokových proměnných pravdivá a pro jiné pravdivostní hodnoty výrokových proměnných nepravdivá, se nazývá *splnitelná formule*.
# ```       

# ## Slovní úlohy

# Pomocí základních znalostí výrokové logiky je možné řešit slovní úlohy (rekreační matematiky).

# ### Příklad 2.2
# 
# Vyšetřováním bylo zjištěno, že nehodu zavinil řidič $B$ nebo $C$. Jestliže nehodu zavinil řidič $C$, zavinil ji i řidič $A$. Nehodu zavinil řidič $B$, jestliže ji zavinil řidič $A$. Jestliže nehodu zavinil řidič $B$, pak ji řidič $C$ nezavinil. Podaří se vám rozhodnout, kdo z řidičů nehodu zavinil?
# 
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# Výrok $A$: *Nehodu zavinil řidič $A$*
# Výrok $B$: *Nehodu zavinil řidič $B$*
# Výrok $C$: *Nehodu zavinil řidič $C$*
# 
# Podle zadání musí platit: $V = (B \lor C) \land (C \Rightarrow A) \land (A \Rightarrow B) \land (B \Rightarrow C') .
# 
# | $A$ | $B$ | $C$ | $C'$| $B \lor C$ | $C \Rightarrow A$ | $A \Rightarrow B$| $B\Rightarrow C'$|$V$|
# |-----|-----|-----|-----|-----|-----|-----|-----|-----|
# | 0   | 0   | 0   | 1 |0|1|1|1|0| 
# | 0   | 0   | 1   | 0 |1|0|1|1|0| 
# | **0**   | **1**   | **0**   | **1** |**1**|**1**|**1**|**1**|**1**|
# | 0   | 1   | 1   | 0 |1|0|1|0|0|
# | 1   | 0   | 0   | 1 |0|1|0|1|0|
# | 1   | 0   | 1   | 0 |1|1|0|1|0|
# | **1**   | **1**   | **0**   | **1** |**1**|**1**|**1**|**1**|**1**|
# | 1   | 1   | 1   | 0 |1|1|1|0|0|
# 
# Nehodu zavinil buď řidič $B$ sám nebo s řidičem $A$.
# ```   

# ## Logicky ekvivalentní výrokové formule

# ```{admonition} Definice 2.3
# Výroková formule $X$ je *logicky ekvivalentní* s výrokovou formulí $Y$ právě tehdy, když $X\Leftrightarrow Y$ je tautologií. 
# 
# Logicky ekvivalentní formule $X, Y$ budeme zapisovat $X\sim Y$.
# ```

# ### Příklad 2.3
# 
# Ověřte, že platí $(A \Rightarrow B) \sim (A' \lor B)$.
# 
# 
# ```{admonition} Zobrazit řešení
# :class: dropdown warning
# | $A$ | $B$ | $A'$ |$A \Rightarrow B$|$A'\lor B$ |
# |-----|-----|--|--|--|
# | 0   | 0   |1|1|1|
# | 0   | 1   |1|1|1|
# | 1   | 0   |0|0|0|
# | 1   | 1   |0|1|1|
# 
# Výrokové formule nejsou logicky ekvivalentní.
# 
# ```   
# 
# 
# 

# ```{admonition} Poznámka 2.3
# :class: hint
# **Významné tautologie výrokové logiky**
# 
# 1. $(A\land B)\sim (B\land A)$ ... komutativnost konjunkce 
# 2. $(A\lor B)\sim (B\lor A)$ ... komutativnost disjunkce 
# 3.$(A\land B) \land C\sim A \land (B\land C)$ ... asociativnost konjunkce 
# 4. $(A\lor B) \lor C\sim A \lor (B\lor C)$ ... asociativnost disjunkce 
# 5. $(A\land B) \lor C\sim (A \lor C) \land (B\lor C)$ ... distributivnost disjunkce vzhledem ke konjunkci
# 6. $(A\lor B)\land C \sim (A\land C)\lor (B\land C)$ ... distributivnost konjunkce vzhledem k disjunkci 
# 7. $(A')'\sim A$ ... zákon dvojité negace 
# 8. $A \lor A' \sim 1$ ... zákon vyloučení třetí možnosti 
# 9. $A\land A' \sim 0$ ... zákon sporu 
# 10. $(A\Rightarrow B)\sim (A'\lor B)$ ... nahrazení implikace pomocí negace a disjunkce
# 11. $(A\Leftrightarrow B)\sim (A\Rightarrow B)\land (B \Rightarrow A)$ ... nahrazení ekvivalence pomocí implikace a konjunkce 
# 12. $(A\lor B)' \sim (A' \land B')$ ... de Morganův zákon 
# 13. $(A\land B)'\sim (A' \lor B')$ ... de Morganův zákon 
# 14. $(A \Rightarrow B) \sim  (B' \Rightarrow A')$ ... nahrazení implikace implikací obměněnou
# ```       
# 
# 
# 
